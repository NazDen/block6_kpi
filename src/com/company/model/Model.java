package com.company.model;

import com.company.entities.Note;
import com.company.exeptions.LoginExistsExeption;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 26.09.2017.
 */
public class Model {

private final List<Note> notebook;

    public Model() {
        this.notebook = new ArrayList<>();
        notebook.add(new Note("Назар", "testTest"));
    }

   public boolean loginExists(Note note) throws LoginExistsExeption {
      if(notebook.contains(note)){
          throw new LoginExistsExeption(String.format("Користувач з логіном:\"%s\" уже існує, виберіть інший логін.", note.getLogin()));
      }
      return notebook.contains(note);
   }

   public void addNote(Note note){
        notebook.add(note);
   }
}
