package com.company.exeptions;

public class LoginExistsExeption extends Exception {

    public LoginExistsExeption(String message) {
        super(message);
    }
}
