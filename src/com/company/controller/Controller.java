package com.company.controller;

import com.company.entities.Note;
import com.company.exeptions.LoginExistsExeption;
import com.company.model.Model;
import com.company.view.View;

import java.util.Scanner;

/**
 * Created by student on 26.09.2017.
 */
public class Controller {
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {
        view.printMessage("Тестовий логін: testTest");
        Scanner sc = new Scanner(System.in);

        InputNoteNoteBook inputNoteNoteBook =
                new InputNoteNoteBook(view, sc);
        inputNoteNoteBook.inputNote();

        boolean flag = true;
        while (flag){
        try {
            Note note = new Note(inputNoteNoteBook.getFirstName(), inputNoteNoteBook.getLogin());
            flag = model.loginExists(note);
            model.addNote(note);
        } catch (LoginExistsExeption e) {
            view.printMessage(e.getMessage());
            inputNoteNoteBook.inputLogin();
        }
        }


    }
}
